using UnityEngine;
using UnityEngine.Events;

public class CharacterController2D : MonoBehaviour
{
    [SerializeField] private float m_JumpForce = 400f;                          // Amount of force added when the player jumps.
    [Range(0, .3f)] [SerializeField] private float m_MovementSmoothing = .05f;  // How much to smooth out the movement
    [SerializeField] private bool m_AirControl = false;                         // Whether or not a player can steer while jumping;
    [SerializeField] private LayerMask m_WhatIsGround;                          // A mask determining what is ground to the character
    [SerializeField] private Transform m_GroundCheck;                           // A position marking where to check if the player is grounded.

    const float k_GroundedRadius = .2f; // Radius of the overlap circle to determine if grounded
    public bool m_Grounded;            // Whether or not the player is grounded.
    private Rigidbody2D m_Rigidbody2D;
    [SerializeField] private bool m_FacingRight = true;  // For determining which way the player is currently facing.
    private Vector3 m_Velocity = Vector3.zero;
    private SpriteRenderer myRenderer;
    [SerializeField] GameObject myUI;
    private Animator m_Animator;
    [SerializeField] bool Debugging = false;

    private void Awake()
    {
        m_Rigidbody2D = GetComponent<Rigidbody2D>();
        myRenderer = GetComponent<SpriteRenderer>();
        m_Animator = GetComponent<Animator>();
    }

    private void FixedUpdate()
    {
        m_Grounded = false;

        // The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
        // This can be done using layers instead but Sample Assets will not overwrite your project settings.
        Collider2D[] colliders = Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius, m_WhatIsGround);
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject != gameObject)
            {
                m_Grounded = true;
            }
        }
        m_Animator.SetBool("Grounded", m_Grounded);
    }


    public void Move(float move, bool jump)
    {
        //only control the player if grounded or airControl is turned on
        if (m_Grounded || m_AirControl)
        {
            // Move the character by finding the target velocity
            Vector3 targetVelocity = new Vector2(move * 10f, m_Rigidbody2D.velocity.y);
            // And then smoothing it out and applying it to the character
            m_Rigidbody2D.velocity = Vector3.SmoothDamp(m_Rigidbody2D.velocity, targetVelocity, ref m_Velocity, m_MovementSmoothing);

            // If the input is moving the player right and the player is facing left...
            if (move > 0 && !m_FacingRight)
            {
                // ... flip the player.
                Flip();
            }
            // Otherwise if the input is moving the player left and the player is facing right...
            else if (move < 0 && m_FacingRight)
            {
                // ... flip the player.
                Flip();
            }
        }
        // If the player should jump...
        if (m_Grounded && jump)
        {
            // Add a vertical force to the player.
            m_Grounded = false;
            m_Rigidbody2D.AddForce(new Vector2(0f, m_JumpForce));
        }
    }


    public void Flip()
    {
        // Switch the way the player is labelled as facing.
        m_FacingRight = !m_FacingRight;

        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;

        Vector3 tempScale = myUI.transform.localScale;
        tempScale.x *= -1;

        myUI.transform.localScale = tempScale;

        //myRenderer.flipY = !m_FacingRight;
    }

    public void UpdateCharacterFacing(Freak.Facing facing)
    {
        if (m_FacingRight && facing == Freak.Facing.Left)
        {
            Flip();
        }
        else if (!m_FacingRight && facing == Freak.Facing.Right)
        {
            Flip();
        }
    }

    public void Launch(int direction, float power)
    {
        float tempPower = direction * power;
        Vector2 tempVec = new Vector2(tempPower, 0);
        m_Rigidbody2D.velocity = Vector2.zero;
        m_Rigidbody2D.AddForce(tempVec);
    }


    public void Animation_BasicAttack()
    {
        m_Animator.SetTrigger("BasicAttack");
        if (Debugging)
        {
            Debug.Log("Called Animation_BasicAttack");

        }
    }

    public void Animation_MutationAttack()
    {
        m_Animator.SetTrigger("MutationAttack");
        if (Debugging)
        {
            Debug.Log("Called Animation_MutationAttack");

        }
    }

    public void Animation_Movement(float velocity)
    {
        m_Animator.SetFloat("Velocity", velocity);
    }

    public void Animation_GetHit()
    {
        m_Animator.SetTrigger("GetHit");
    }

    public void Animation_Block(bool value)
    {
        m_Animator.SetBool("Block", value);
    }

    public void Animation_Invulnerable(bool value)
    {
        m_Animator.SetBool("Invulnerable", value);
    }
}
