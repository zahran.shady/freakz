﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartFightTimer : MonoBehaviour
{
    public float timer = 4.0f;
    float _timer = 0f;
    bool isCounting = false;
    public GameObject ui_startfighttimer;
    // Start is called before the first frame update
    void Start()
    {
        ui_startfighttimer.SetActive(true);
        isCounting = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (isCounting)
        {
            if (_timer >= timer)
            {
                isCounting = false;
                _timer -= timer;
                UI_Gameplay.instance.isCounting = true;
                ui_startfighttimer.SetActive(false);
                GameManager.instance.ChangeActionMap("Fighting");
            }
            else
            {
                _timer += Time.deltaTime;
            }

        }
    }
}
