﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.InputSystem;

public class MainMenu : MonoBehaviour
{
    public Image settingsBackround;
    public Button playBut;
    public Button settBut;
    public Button quitBut;
    public Button contBut;
    PlayerController p1;
    PlayerController p2;
    public GameObject p1Act;
    public GameObject p2Act;
    public GameObject playerInit;
    public GameObject AMainMenu;
    public Image keyboardEnter;
    public Image controllerPlay;
    public Sprite keyboardEnterHigh;
    public Sprite controllerPlayHigh;

    float cdPlayerInit = 0f;

    private void Start()
    {
        searchForPlayers();
        if (p1 != null && p2 != null)
        {
            AMainMenu.SetActive(true);
            playerInit.SetActive(false);
            playBut.Select();
        }
    }

    private void FixedUpdate()
    {
        if (playerInit.activeInHierarchy == false)
        {
            return;
        }
        if (p1 != null && p2 != null)
        {
            PlayerReady(0);
            PlayerReady(1);
            cdPlayerInit += Time.deltaTime;
            if (cdPlayerInit >= 1f)
            {
                AMainMenu.SetActive(true);
                playerInit.SetActive(false);
                playBut.Select();
            }
        }
        else if (p1 != null && p2 == null)
        {
            PlayerReady(0);
            searchForPlayers();
        }
        else if (p1 == null || p2 == null) searchForPlayers();
    }

    public void PlayerReady(int playerID)
    {
        switch (playerID)
        {
            case 0:
                p1Act.SetActive(true);
                PlayerInput p1PlayerInput = p1.GetComponent<PlayerInput>();
                if (p1PlayerInput.currentControlScheme == "Keyboard and mouse") keyboardEnter.overrideSprite = keyboardEnterHigh;
                if (p1PlayerInput.currentControlScheme == "Gamepad") controllerPlay.overrideSprite = controllerPlayHigh;
                Debug.Log("Player1 Ready");
                break;
            case 1:
                p2Act.SetActive(true);
                PlayerInput p2PlayerInput = p2.GetComponent<PlayerInput>();
                if (p2PlayerInput.currentControlScheme == "Keyboard and mouse") keyboardEnter.overrideSprite = keyboardEnterHigh;
                if (p2PlayerInput.currentControlScheme == "Gamepad") controllerPlay.overrideSprite = controllerPlayHigh;
                Debug.Log("Player2 Ready");
                break;
            default:
                break;
        }

    }

    public void searchForPlayers()
    {
        Debug.Log("Searching For Players");
        PlayerController[] tempPC = FindObjectsOfType<PlayerController>();
        for (int i = 0; i < tempPC.Length; i++)
        {
            if (tempPC[i].pNamePub == "Player1")
            {
                p1 = tempPC[i];
                Debug.Log("Player1 Found");
            }
            else if (tempPC[i].pNamePub == "Player2")
            {
                p2 = tempPC[i];
                Debug.Log("Player2 Found");
            }
        }
    }

    public void Play()
    {
        GameManager.instance.Button_Play();
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void Settings()
    {
        playBut.gameObject.SetActive(false);
        settBut.gameObject.SetActive(false);
        quitBut.gameObject.SetActive(false);
        settingsBackround.gameObject.SetActive(true);
        contBut.Select();
    }

    public void changeActiveButton(int index)
    {
        switch (index)
        {
            case 0:
                
                return;
        }
    }
}
