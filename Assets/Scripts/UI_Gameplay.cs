﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UI_Gameplay : MonoBehaviour
{
    public static UI_Gameplay instance;

    [SerializeField] GameObject player1Win_thefool, player2Win_thefool, player1Win_reptile, player2Win_reptile, timeIsUP;
    public Image p1HealthBar;
    public Image p2HealthBar;
    public Image p1ChargeBar;
    public Image p2ChargeBar;
    public Image p1Icon;
    public Image p2Icon;
    public Image p1NameTag, p2NameTag;
    public Sprite theFoolSprite;
    public Sprite reptileSprite;
    public Sprite theFoolNameTag, reptileNameTag;
    public Camera mainCam;
    public PlayerController p1;
    public PlayerController p2;
    public float p1x;
    public float p2x;
    public float dist;
    public Vector3 mCamNextPos;
    public TextMeshProUGUI timerText;
    public int gameDuration = 90;
    float t = 0;
    public bool isCounting = false;
    public bool isDebugging = false;
    bool calledTimeIsUp = false;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        PlayerController[] tempPC = FindObjectsOfType<PlayerController>();
        for (int i = 0; i < tempPC.Length; i++)
        {
            if (tempPC[i].pNamePub == "Player1")
            {
                p1 = tempPC[i];
            }
            else if (tempPC[i].pNamePub == "Player2")
            {
                p2 = tempPC[i];
            }
        }
        if (p1 != null && p2 != null) Debug.Log("Players have been recognized");

        if (p1.chosenCharactersList[0].GetComponent<Freak>().name == "TheFool")
        {
            p1Icon.overrideSprite = theFoolSprite;
            p1NameTag.overrideSprite = theFoolNameTag;
        }
        else
        {
            p1Icon.overrideSprite = reptileSprite;
            p1NameTag.overrideSprite = reptileNameTag;
        }
        if (p2.chosenCharactersList[0].GetComponent<Freak>().name == "TheFool")
        {
            p2Icon.overrideSprite = theFoolSprite;
            p2NameTag.overrideSprite = theFoolNameTag;
        }
        else
        {
            p2Icon.overrideSprite = reptileSprite;
            p2NameTag.overrideSprite = reptileNameTag;
        }
    }

    private void Update()
    {
        if (isCounting)
        {
            if (gameDuration > 0)
            {
                t += Time.deltaTime;
                if (t >= 1)
                {
                    t = 0;
                    gameDuration--;
                    timerText.text = gameDuration.ToString();
                }
            }
            else if (!calledTimeIsUp)
            {
                calledTimeIsUp = true;
                TimeIsUp();
                GameManager.instance.SetWinner(0);
                GameManager.instance.DeclareWinner();
                if (isDebugging)
                {
                    Debug.Log("time is up people!");
                }
            }
        }



    }

    private void LateUpdate()
    {
        //Vector3 mCamCurrPos = mainCam.transform.position;
        //p1x = p1.transform.position.x;
        //p2x = p2.transform.position.x;
        //dist = Mathf.Abs(p2x - p1x);
        //float zoomMag = dist / 18f;
        //mainCam.orthographicSize = 4f + (1f * zoomMag);
        //if (p1x < p2x)
        //{
        //    mCamNextPos = new Vector3(p1x + (dist / 2), mCamCurrPos.y, mCamCurrPos.z);
        //}
        //else
        //{
        //    mCamNextPos = new Vector3(p1x - (dist / 2), mCamCurrPos.y, mCamCurrPos.z);
        //}
        //if (mCamNextPos.x >= 1.66) mCamNextPos.x = 1.65f;
        //else if (mCamNextPos.x <= -1.66f) mCamNextPos.x = -1.65f;
        //mainCam.transform.position = Vector3.Lerp(mCamCurrPos, mCamNextPos, 10f * Time.deltaTime);
    }

    public void Player1Win(string winner)
    {
        if (winner == "TheFool")
        {
            player1Win_thefool.SetActive(true);
        }
        else if (winner == "Reptile")
        {
            player1Win_reptile.SetActive(true);
        }

    }

    public void Player2Win(string winner)
    {
        if (winner == "TheFool")
        {
            player2Win_thefool.SetActive(true);
        }
        else if (winner == "Reptile")
        {
            player2Win_reptile.SetActive(true);
        }

    }

    public void TimeIsUp()
    {
        timeIsUP.SetActive(true);
    }

    public Image GetP1HealthBar()
    {
        Debug.Log("P1Healthbar has been requested");
        return p1HealthBar;
    }

    public Image GetP2HealthBar()
    {
        Debug.Log("P2Healthbar has been requested");
        return p2HealthBar;
    }

    public Image GetP1ChargeBar()
    {
        return p1ChargeBar;
    }

    public Image GetP2ChargeBar()
    {
        return p2ChargeBar;
    }
}



