﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    bool Debugging = false;
    [SerializeField]
    Vector3 whereToSpawn;
    [SerializeField]
    Vector3 currentPosition;
    [SerializeField]
    string playerName;


    public List<GameObject> chosenCharactersList;

    public List<GameObject> spawnedCharactersList;

    [SerializeField] int currentHeroIndex = 0;


    float horizontalMove = 0f;

    public CharacterController2D controller;
    Freak myFreak;
    public Image p1HealthBar;
    public Image p2HealthBar;
    public Image p1ChargeBar;
    public Image p2ChargeBAr;
    public string pNamePub;

    GameObject spawnedCharacterObj;
    [SerializeField] bool spawned = false;
    GameObject spawnedCharacters;


    PlayerInput myPlayerInput;
    string myDeviceName;

    private PlayerState currentstate;

    public float immuneTimer = 1.0f;
    public float stunnedTimer = 1.0f;
    private float m_immuneTimer = 0f;
    public bool isInvulnerable = false;

    public int stunCounter = 2;
    //bool 
    private int _stunCounter = 0;
    //private float _stunTimer = 0f;

    public CharacterSelectMenu m_characterselectmenu;
    [SerializeField] private Freak.Facing myFacing;

    public Freak.Facing MyFacing
    {
        get
        {
            return myFacing;
        }
        set
        {
            myFacing = value;
        }
    }

    public delegate void OnVariableChangeDelegate(PlayerState newVal);
    public event OnVariableChangeDelegate OnStateChange;

    public PlayerState Currentstate
    {
        get
        { return currentstate; }
        set
        {
            if (currentstate == value)
            {
                return;
            }
            else
            {
                currentstate = value;
                if (OnStateChange != null)
                {
                    OnStateChange(currentstate);
                }
            }
            currentstate = value;
        }
    }

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);

        myPlayerInput = GetComponent<PlayerInput>();
        myDeviceName = myPlayerInput.devices[0].displayName;



        playerName = "Player" + (myPlayerInput.user.id);
        pNamePub = playerName;

        if (Debugging)
        {
            Debug.Log("Player name: " + playerName);
            Debug.Log("My Device: " + myDeviceName);
        }
        Currentstate = PlayerState.Active;
        if (myFreak != null)
        {
            myFreak.DebugTextDisplay(nameof(PlayerState.Active));
        }


        ResetPlayer();
        OnStateChange += Player_OnStateChange;

        SoundManager.instance.Play_UISound();
    }

    private void Start()
    {
        GameManager.instance.AddPlayer(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        //if we spawned a freak, then start feeding the horizontal movement value
        if (spawned)
        {
            switch (Currentstate)
            {
                case PlayerState.Active:
                    myFreak.Move(horizontalMove);
                    myFreak.stunIndicator.SetActive(false);
                    myFreak.immuneIndicator.SetActive(false);
                    if (Debugging)
                    {
                        Debug.Log("inside active");
                    }
                    break;
                case PlayerState.Blocking:
                    if (Debugging)
                    {
                        Debug.Log("inside blocking");
                    }
                    break;
                case PlayerState.Dead:
                    if (Debugging)
                    {
                        Debug.Log("inside dead");
                    }
                    break;
                case PlayerState.Stunned:
                    if (!myFreak.immuneIndicator.activeInHierarchy)
                    {
                        myFreak.stunIndicator.SetActive(true);
                    }
                    else
                    {
                        myFreak.stunIndicator.SetActive(false);
                    }
                    
                    if (Debugging)
                    {
                        Debug.Log("inside stunned");
                    }
                    break;
                default:
                    break;
            }
            if (myFreak != null) this.transform.position = myFreak.transform.position;

            if (isInvulnerable)
            {
                myFreak.immuneIndicator.SetActive(true);
                if (m_immuneTimer < immuneTimer)
                {
                    m_immuneTimer += Time.deltaTime;
                }
                else
                {
                    isInvulnerable = false;
                    m_immuneTimer -= immuneTimer;
                    myFreak.immuneIndicator.SetActive(false);
                }
            }


        }

    }

    //spawing freak to control
    void OnSpawnPlayer()
    {
        if (!spawned)
        {
            InitCharacters();
        }
    }

    void OnSpawn()
    {

    }

    void OnJump()
    {
        if (spawned)
        {
            if (Currentstate == PlayerState.Active)
            {
                myFreak.Jump();
            }

        }
        if (Debugging)
        {
            Debug.Log(playerName + " Jump!");
        }
    }

    void OnBasicAttack()
    {
        if (Currentstate == PlayerState.Active ||
            Currentstate == PlayerState.InJumpAnimation)
        {
            myFreak.BasicAttack();
            if (Debugging)
            {
                Debug.Log(playerName + " Basic attack!");
            }
        }


    }

    void OnMove(InputValue value)
    {
        horizontalMove = value.Get<float>();
        if (horizontalMove > 0 && MyFacing == Freak.Facing.Left)
        {
            MyFacing = Freak.Facing.Right;
        }
        else if (horizontalMove < 0 && MyFacing == Freak.Facing.Right)
        {
            MyFacing = Freak.Facing.Left;
        }
        if (Debugging)
        {
            Debug.Log(playerName + " Moving! value:" + horizontalMove);
        }
    }

    void OnSwitch()
    {
        int tempIndex = GetNextHeroIndex();
        if (tempIndex != -1 && Currentstate == PlayerState.Active)
        {
            currentPosition = myFreak.gameObject.GetComponent<Transform>().position;

            spawnedCharacterObj.SetActive(false);

            spawnedCharacterObj = spawnedCharactersList[tempIndex];

            spawnedCharacterObj.transform.position = currentPosition;

            myFreak = spawnedCharacterObj.GetComponent<Freak>();

            if (myPlayerInput.user.id == 2)
            {
                myFreak.ui_health = p2HealthBar;
                myFreak.ui_charge = p2ChargeBAr;
            }
            else
            {
                myFreak.ui_health = p1HealthBar;
                myFreak.ui_charge = p1ChargeBar;
            }
            myFreak.Health -= 1;
            myFreak.Health += 1;
            myFreak.Charge += 1;
            myFreak.Charge -= 1;

            spawnedCharacterObj.GetComponent<CharacterController2D>().UpdateCharacterFacing(MyFacing);

            spawnedCharacterObj.SetActive(true);
        }


        if (Debugging)
        {
            Debug.Log(playerName + " Switch!");
        }


    }

    void OnBlock(InputValue value)
    {
        float tempValue = (float)(value.Get());
        Debug.Log(value.Get());
        if (Currentstate == PlayerState.Blocking && tempValue < 0.5f)
        {
            myFreak.Block(false);
            if (Debugging)
            {
                Debug.Log(playerName + " unblock!");
            }
        }
        else if (Currentstate == PlayerState.Active && tempValue > 0.5f)
        {
            myFreak.Move(0f);
            myFreak.Block(true);
            if (Debugging)
            {
                Debug.Log(playerName + " block!");
            }

        }


    }

    void OnMutate()
    {
        if (Currentstate == PlayerState.Active)
        {
            myFreak.MutationAttack();
            if (Debugging)
            {
                Debug.Log(playerName + " Mutation attack!");
            }
        }

    }

    //not an ability, but an immune state
    void OnInvulnerable()
    {

    }

    void OnAnyButton()
    {
        SoundManager.instance.Play_UISound();
    }

    void OnSkip()
    {
        GameManager.instance.RestartGame();
    }

    public void InitCharacters()
    {
        //initial spawn
        //spawn my list of characters
        //make the first one active
        //keep tracking the position for switching character at the same position
        spawnedCharacters = new GameObject
        {
            name = playerName + " characters"
        };
        spawnedCharacters.transform.position = Vector3.zero;
        for (int i = 0; i < chosenCharactersList.Count; i++)
        {
            GameObject tempObj = Instantiate(chosenCharactersList[i], whereToSpawn, Quaternion.identity, spawnedCharacters.transform);
            tempObj.SetActive(false);
            spawnedCharactersList.Add(tempObj);
            tempObj.GetComponent<Freak>().InitFreak(i, this);
        }

        UI_Gameplay tempUIG = FindObjectOfType<UI_Gameplay>();
        if (tempUIG != null && (p1HealthBar == null || p2HealthBar == null) && (p1ChargeBar == null || p2ChargeBAr == null))
        {
            p1HealthBar = tempUIG.GetP1HealthBar();
            p2HealthBar = tempUIG.GetP2HealthBar();
            p1ChargeBar = tempUIG.GetP1ChargeBar();
            p2ChargeBAr = tempUIG.GetP2ChargeBar();
            if (Debugging)
            {
                if (p1HealthBar != null && p2HealthBar != null) Debug.Log("Healthbars have been loaded");
            }
        }

        spawned = true;
        spawnedCharacterObj = spawnedCharactersList[0];
        myFreak = spawnedCharacterObj.GetComponent<Freak>();
        spawnedCharacterObj.transform.position = GameManager.instance.RequestSpawnPoint(myPlayerInput.user.id);
        if (myPlayerInput.user.id == 2)
        {
            MyFacing = Freak.Facing.Left;
            myFreak.FlipCharacter();
            myFreak.ui_health = p2HealthBar;
            myFreak.ui_charge = p2ChargeBAr;
        }
        else
        {
            MyFacing = Freak.Facing.Right;
            myFreak.ui_health = p1HealthBar;
            myFreak.ui_charge = p1ChargeBar;
        }
        myFreak.Health -= 1;
        myFreak.Health += 1;
        myFreak.Charge += 1;
        myFreak.Charge -= 1;
        spawnedCharacterObj.SetActive(true);
    }



    int GetNextHeroIndex()
    {
        if (spawnedCharactersList.Count == 1)
        {
            //no switching
            //return -1
            return -1;
        }
        else if (currentHeroIndex == spawnedCharactersList.Count - 1)
        {
            //return 0
            currentHeroIndex = 0;
            return currentHeroIndex;
        }
        else
        {
            //return index++
            currentHeroIndex++;
            return currentHeroIndex;
        }
    }


    public void KillCharacter(GameObject freak)
    {
        currentPosition = myFreak.gameObject.transform.position;
        spawnedCharactersList.Remove(freak);
        Destroy(spawnedCharacterObj);

        if (spawnedCharactersList.Count == 0)
        {
            Die();
        }
        else
        {
            ActivateNextCharacter();
        }
    }

    void ActivateNextCharacter()
    {
        spawnedCharacterObj = spawnedCharactersList[0];

        spawnedCharacterObj.transform.position = currentPosition;

        myFreak = spawnedCharacterObj.GetComponent<Freak>();

        if (myPlayerInput.user.id == 2)
        {
            myFreak.ui_health = p2HealthBar;
            myFreak.ui_charge = p2ChargeBAr;
        }
        else
        {
            myFreak.ui_health = p1HealthBar;
            myFreak.ui_charge = p1ChargeBar;
        }
        myFreak.Health -= 1;
        myFreak.Health += 1;
        myFreak.Charge += 1;
        myFreak.Charge -= 1;

        spawnedCharacterObj.GetComponent<CharacterController2D>().UpdateCharacterFacing(MyFacing);


        spawnedCharacterObj.SetActive(true);

        currentHeroIndex = 0;

        isInvulnerable = true;

        if (Debugging)
        {
            Debug.Log(playerName + " Activate next character!");
        }
    }

    void Die()
    {
        Currentstate = PlayerState.Dead;
        Debug.Log(playerName + " died!");
        if (myPlayerInput.user.id == 1)
        {
            GameManager.instance.SetWinner(2);
        }
        else if (myPlayerInput.user.id == 2)
        {
            GameManager.instance.SetWinner(1);
        }
        GameManager.instance.DeclareWinner();
        //GameManager.instance.RestartGame();
    }

    public enum PlayerState
    {
        Active,
        Dead,
        Stunned,
        InHitAnimation,
        InJumpAnimation,
        Blocking
    }

    void OnReady()
    {

        if (myPlayerInput.user.id == 1 && CharacterSelectMenu.instance.P1Picking)
        {
            CharacterSelectMenu.instance.RdyClickP1();
        }
        else if (myPlayerInput.user.id == 2 && !CharacterSelectMenu.instance.P1Picking)
        {
            CharacterSelectMenu.instance.RdyClickP2();
        }
    }

    void OnSwitchCharacter(InputValue value)
    {
        float tempValue = value.Get<float>();
        if (tempValue > 0)
        {
            CharacterSelectMenu.instance.ArrowClick(true);
        }
        else
        {
            CharacterSelectMenu.instance.ArrowClick(false);
        }
    }


    public void ResetPlayer()
    {
        spawned = false;
        Currentstate = PlayerState.Active;
        if (myFreak != null)
        {
            myFreak.DebugTextDisplay("Active");
        }

        whereToSpawn = Vector3.zero;
        currentPosition = Vector3.zero;
        spawnedCharactersList = new List<GameObject>();
        currentHeroIndex = 0;
        controller = null;
        myFreak = null;
        spawnedCharacterObj = null;

        spawnedCharacters = null;
        m_immuneTimer = 0f;
    }




    private void Player_OnStateChange(PlayerState newVal)
    {
        string result = "";
        switch (newVal)
        {
            case PlayerState.Active:
                result = nameof(PlayerState.Active);
                break;
            case PlayerState.Dead:
                result = nameof(PlayerState.Dead);
                break;
            case PlayerState.Stunned:
                result = nameof(PlayerState.Stunned);
                break;
            case PlayerState.InHitAnimation:
                result = nameof(PlayerState.InHitAnimation);
                break;
            case PlayerState.InJumpAnimation:
                result = nameof(PlayerState.InJumpAnimation);
                break;
            case PlayerState.Blocking:
                result = nameof(PlayerState.Blocking);
                break;
            default:
                break;
        }
        if (myFreak != null)
        {
            myFreak.DebugTextDisplay(result);
        }

        //Debug.Log("updated debug text!");
    }

    public void Player_UpdateState(PlayerState state)
    {
        Currentstate = state;
        if (Debugging)
        {
            Debug.Log("updated state to:" + state);
        }
    }

    public void UpdateHitCounter()
    {
        if (_stunCounter < stunCounter)
        {
            _stunCounter++;
            if (Debugging)
            {
                Debug.Log("added hit counter");
            }
            if (_stunCounter == stunCounter)
            {
                isInvulnerable = true;
                _stunCounter = 0;
                if (Debugging)
                {
                    Debug.Log("combo immune initiated");
                }
            }
        }
    }

    void ResetInvulnerableTimer()
    {
        m_immuneTimer -= immuneTimer;
    }

    public void ResetStunCounter()
    {
        _stunCounter = 0;
    }

    public bool IsBlockingSameDirectionAsHit(int direction)
    {
        if (MyFacing == Freak.Facing.Right && direction == -1)
        {
            return true;
        }
        else if (MyFacing == Freak.Facing.Left && direction == 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}
