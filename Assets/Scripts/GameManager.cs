﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem.Controls;
using UnityEngine.InputSystem.LowLevel;
using UnityEngine.InputSystem.Users;
using UnityEngine.InputSystem.Utilities;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;// Start is called before the first frame update
    //public bool inMenu = false;
    public List<GameObject> playersList;
    public string winnerFreakName = "";
    public uint winnerPlayerId;
    private void Awake()
    {
        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        playersList = new List<GameObject>();
    }


    public void AddPlayer(GameObject player)
    {
        playersList.Add(player);
    }


    public void LoadFightingScene(bool runCoroutine, float delayDuration)
    {
        foreach (var player in playersList)
        {
            player.GetComponent<PlayerInput>().SwitchCurrentActionMap("Idle");

        }

        if (runCoroutine)
        {
            StartCoroutine(LoadSceneCor(2, delayDuration));
        }
        else
        {
            SoundManager.instance.StopAllSounds();
            SceneManager.LoadScene(2);
        }
    }

    IEnumerator LoadSceneCor(int sceneIndex, float delay)
    {
        yield return new WaitForSeconds(delay);
        SoundManager.instance.StopAllSounds();
        SceneManager.LoadScene(sceneIndex);
    }

    public void LoadCharacterMenuScene()
    {
        foreach (var player in playersList)
        {
            player.GetComponent<PlayerInput>().SwitchCurrentActionMap("UI_CharacterMenu");

        }
        SceneManager.LoadScene(1);
    }

    public void LoadMainMenuScene()
    {
        foreach (var player in playersList)
        {
            player.GetComponent<PlayerInput>().SwitchCurrentActionMap("UI_MainMenu");

        }
        SceneManager.LoadScene(0);
    }

    public Vector3 RequestSpawnPoint(uint playerid)
    {
        Vector3 spawnpoint;
        switch (playerid)
        {
            case 1:
                spawnpoint = new Vector3(-6f, 0f, 0f);
                break;
            case 2:
                spawnpoint = new Vector3(6f, 0f, 0f);
                break;
            default:
                spawnpoint = Vector3.zero;
                break;
        }
        return spawnpoint;
    }

    public void RestartGame()
    {
        StartCoroutine(Cor_Restart());
    }

    public bool CheckPlayers()
    {
        if (playersList.Count > 1)
        {
            //already have players with me
            return true;
        }
        else
        {
            return false;
        }
    }

    public void Button_Play()
    {
        if (CheckPlayers())
        {
            LoadCharacterMenuScene();
        }
        else
        {
            Debug.Log("players are not initialized");
        }
    }


    public void CheckWinner()
    {
        string result = "";
        int tempCount = 0;
        int healthResult;
        PlayerController playerA = playersList[0].GetComponent<PlayerController>();
        PlayerController playerB = playersList[1].GetComponent<PlayerController>();
        //if both have the same number of characters left
        if (playerA.spawnedCharactersList.Count == playerB.spawnedCharactersList.Count)
        {
            //check health for each character
            healthResult = CheckPlayersHealth();
            switch (healthResult)
            {
                case 0:
                    Debug.Log("Its a draw!");
                    break;
                case 1:
                    Debug.Log("Player 1 wins by health difference");
                    SetWinner(1);
                    DeclareWinner();
                    break;
                case -1:
                    Debug.Log("Player 2 wins by health difference");
                    SetWinner(2);
                    DeclareWinner();
                    break;
                default:
                    break;
            }
        }
        else if (playerA.spawnedCharactersList.Count > playerB.spawnedCharactersList.Count)
        {
            Debug.Log("Player 1 wins by having more characters left");
        }

        else if (playerA.spawnedCharactersList.Count < playerB.spawnedCharactersList.Count)
        {
            Debug.Log("Player 2 wins by having more characters left");
        }

    }

    public int CheckPlayersHealth()
    {
        float playerAHealth = 0f;
        float playerBHealth = 0f;

        for (int i = 0; i < playersList.Count; i++)
        {
            PlayerController tempPlayerController = playersList[i].GetComponent<PlayerController>();
            for (int j = 0; j < tempPlayerController.spawnedCharactersList.Count; j++)
            {
                Freak tempFreak = tempPlayerController.spawnedCharactersList[j].GetComponent<Freak>();
                if (i == 0)
                {
                    playerAHealth += tempFreak.Health / tempFreak.maxHealth;
                }
                else if (i == 1)
                {
                    playerBHealth += tempFreak.Health / tempFreak.maxHealth;
                }
            }
        }

        if (playerAHealth > playerBHealth)
        {
            return 1;
        }
        else if (playerAHealth < playerBHealth)
        {
            return -1;
        }
        else
        {
            return 0;
        }
    }

    // called second
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (scene.name == "FightingScene")
        {
            foreach (var player in playersList)
            {
                player.GetComponent<PlayerController>().InitCharacters();
            }

            CameraManager.instance.initDynamicCamera(playersList[0], playersList[1]);
        }
    }

    void OnEnable()
    {
        //Debug.Log("OnEnable called");
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnDisable()
    {
        //Debug.Log("OnDisable");
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    public void IdleControls()
    {
        foreach (var player in playersList)
        {
            player.GetComponent<PlayerInput>().SwitchCurrentActionMap("Idle");

        }
    }

    public void FightEndControls()
    {
        foreach (var player in playersList)
        {
            player.GetComponent<PlayerInput>().SwitchCurrentActionMap("FightEnd");

        }
    }

    IEnumerator Cor_Restart()
    {
        SoundManager.instance.StopAllSounds();
        UI_Gameplay.instance.isCounting = false;
        yield return new WaitForSeconds(0f);
        foreach (var player in playersList)
        {
            player.GetComponent<PlayerController>().ResetPlayer();
        }
        LoadMainMenuScene();
    }

    public void ChangeActionMap(string mapName)
    {
        foreach (var player in playersList)
        {
            player.GetComponent<PlayerInput>().SwitchCurrentActionMap(mapName);

        }
    }

    public void SetWinner(uint winnerId)
    {
        winnerPlayerId = winnerId;
        foreach (var player in playersList)
        {
            if (player.GetComponent<PlayerInput>().user.id == winnerId)
            {
                if (player.GetComponent<PlayerController>().chosenCharactersList[0].GetComponent<Freak>().Name == "Reptile")
                {
                    winnerFreakName = "Reptile";
                }
                else if (player.GetComponent<PlayerController>().chosenCharactersList[0].GetComponent<Freak>().Name == "TheFool")
                {
                    winnerFreakName = "TheFool";
                }
            }
        }
    }

    public void PlayWinnerVoice()
    {
        if (winnerFreakName == "TheFool")
        {
            SoundManager.instance.Play_FoolWin();
        }
        else if (winnerFreakName == "Reptile")
        {

        }
    }

    public void DeclareWinner()
    {
        FightEndControls();
        UI_Gameplay.instance.isCounting = false;
        if (winnerPlayerId == 1)
        {
            UI_Gameplay.instance.Player1Win(winnerFreakName);
        }
        else if (winnerPlayerId == 2)
        {
            UI_Gameplay.instance.Player2Win(winnerFreakName);
        }
        else
        {
            return;
        }

        SoundManager.instance.Play_VictoryLine(winnerFreakName);

    }
}
