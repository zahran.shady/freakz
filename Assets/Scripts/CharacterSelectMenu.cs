﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem.Controls;
using UnityEngine.InputSystem.LowLevel;
using UnityEngine.InputSystem.Users;
using UnityEngine.InputSystem.Utilities;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CharacterSelectMenu : MonoBehaviour
{
    public static CharacterSelectMenu instance;// Start is called before the first frame update

    public Sprite p1TheFool;
    public Sprite p1Reptilian;
    public Sprite p2TheFool;
    public Sprite p2Reptilian;
    public GameObject FS;
    public GameObject FS2;
    public GameObject P2But;
    public Button P1CharLBut;
    public Button P1CharRBut;
    public Button P2CharLBut;
    public Button P2CharRBut;
    public List<GameObject> charToChooseList;
    public bool P1Picking = true;//Is Player 1 picking
    public bool P1Picked = false;//Is true when Player 1 picked
    public bool P2Picked = false;//Is true when Player 2 picked
    int charIndex = 0;
    public int maxCharacters = 2;
    PlayerController p1;
    PlayerController p2;

    public void Back()
    {
        GameManager.instance.LoadMainMenuScene();
    }

    private void Start()
    {
        PlayerController[] tempPC = FindObjectsOfType<PlayerController>();
        for (int i = 0; i < tempPC.Length; i++)
        {
            if (tempPC[i].pNamePub == "Player1")
            {
                p1 = tempPC[i];
            }
            else if (tempPC[i].pNamePub == "Player2")
            {
                p2 = tempPC[i];
            }
        }
        if (p1 != null && p2 != null)
        {
            Debug.Log("Players have been recognized");
            p1.chosenCharactersList[0] = null;
            p2.chosenCharactersList[0] = null;
            //p1.chosenCharactersList[1] = reptilian;
            //p2.chosenCharactersList[1] = reptilian;
            //p1.chosenCharactersList[2] = bomber;
            //p2.chosenCharactersList[2] = bomber;
        }
        charIndex = maxCharacters - 1;
        ArrowClick(true);
    }

    public void RdyClickP1()
    {
        if (P1Picked)
        {
            P1Picking = false;
            var rdyBut = GetComponentsInChildren<Button>();
            for (int i = 0; i < rdyBut.Length; i++)
            {
                if (rdyBut[i].gameObject.name == "Rdy P1")
                {
                    rdyBut[i].interactable = false;
                }
            }

            p1.chosenCharactersList[0] = charToChooseList[charIndex];
            if (charToChooseList[charIndex].name == "TheFool")
            {
                SoundManager.instance.Play_FoolLaugh();
            }
            else
            {
                SoundManager.instance.Play_ReptileLick();
            }

            P2But.gameObject.SetActive(true);
            P1CharLBut.gameObject.SetActive(false);
            P1CharRBut.gameObject.SetActive(false);
            P2CharLBut.gameObject.SetActive(true);
            P2CharRBut.gameObject.SetActive(true);
            charIndex = maxCharacters - 1;
            ArrowClick(true);
            P2CharRBut.Select();
        }
    }

    public void RdyClickP2()
    {
        if (P2Picked)
        {
            p2.chosenCharactersList[0] = charToChooseList[charIndex];
            if (charToChooseList[charIndex].name == "TheFool")
            {
                SoundManager.instance.Play_FoolLaugh();
            }
            else
            {
                SoundManager.instance.Play_ReptileLick();
            }
            GameManager.instance.LoadFightingScene(true, 2.0f);
        }
    }

    public void ArrowClick(bool forward)
    {
        Image rend;
        if (P1Picking)
        {
            P1Picked = true;
            FS.SetActive(true);
            rend = FS.gameObject.GetComponent<Image>();
            if (forward) charIndex++;
            else charIndex--;
            if (charIndex > maxCharacters - 1) charIndex = 0;
            if (charIndex < 0) charIndex = maxCharacters - 1;
        }
        else
        {
            P2Picked = true;
            FS2.SetActive(true);
            rend = FS2.gameObject.GetComponent<Image>();
            if (forward) charIndex++;
            else charIndex--;
            if (charIndex > maxCharacters - 1) charIndex = 0;
            if (charIndex < 0) charIndex = maxCharacters - 1;
        }
        switch (charIndex)
        {
            case 0:
                if (P1Picking) rend.sprite = p1TheFool;
                else rend.sprite = p2TheFool;
                break;
            case 1:
                if (P1Picking) rend.sprite = p1Reptilian;
                else rend.sprite = p2Reptilian;
                break;
            case 2:
                break;
            default:
                break;

        }
    }

    void OnAnyButton()
    {
        SoundManager.instance.Play_UISound();
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }
}
