﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    public static CameraManager instance;

    public GameObject targetGroup;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }


    public void initDynamicCamera(GameObject player1, GameObject player2)
    {
        Cinemachine.CinemachineTargetGroup.Target player1Target = new Cinemachine.CinemachineTargetGroup.Target
        {
            target = player1.transform,
            weight = 1f,
            radius = 0f
        };

        Cinemachine.CinemachineTargetGroup.Target player2Target = new Cinemachine.CinemachineTargetGroup.Target
        {
            target = player2.transform,
            weight = 1f,
            radius = 0f
        };

        Cinemachine.CinemachineTargetGroup.Target[] targetList = new Cinemachine.CinemachineTargetGroup.Target[]
        {
            player1Target, player2Target
        };

        targetGroup.GetComponent<Cinemachine.CinemachineTargetGroup>().m_Targets = targetList;
    }

}
