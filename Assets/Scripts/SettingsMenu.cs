﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsMenu : MonoBehaviour
{
    bool inControl = false;
    bool inSound = false;
    public Image controlImage;
    public Image soundMenuImage;
    public Button controlBut;
    public Slider sSlider;
    public Button playBut;
    public Button settBut;
    public Button quitBut;

    void Start()
    {
        
    }

    void Update()
    {
        if ((Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Joystick1Button1)) && inControl == false && inSound == false)
        {
            playBut.gameObject.SetActive(true);
            settBut.gameObject.SetActive(true);
            quitBut.gameObject.SetActive(true);
            gameObject.SetActive(false);
            playBut.Select();
        }
        else if ((Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Joystick1Button1) && inSound == true))
        {
            inSound = false;
            soundMenuImage.gameObject.SetActive(false);
            controlBut.gameObject.SetActive(true);
            controlBut.Select();
        }
        else if ((Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Joystick1Button1) && inControl == true))
        {
            inControl = false;
            controlImage.gameObject.SetActive(false);
            controlBut.Select();
        }
    }

    public void ControlClick()
    {
        inControl = true;
        controlImage.gameObject.SetActive(true);
    }

    public void SoundClick()
    {
        inSound = true;
        controlBut.gameObject.SetActive(false);
        soundMenuImage.gameObject.SetActive(true);
    }

    public void OnSliderChange()
    {
        SoundManager sManager = SoundManager.instance;
        sManager.GetComponent<AudioSource>().volume = (sSlider.value);
    }
}
