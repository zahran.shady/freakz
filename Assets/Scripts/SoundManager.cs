﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SoundManager : MonoBehaviour
{
    public AudioClip menuClip, fightingClip;
    public static SoundManager instance;
    public AudioSource uiSound, foolWins, foolLaugh, reptileLick, player1Wins, player2Wins, reptileWins;
    private void Awake()
    {
        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    void OnEnable()
    {
        //Debug.Log("OnEnable called");
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode sceneMode)
    {
        if (scene.name == "FightingScene")
        {
            GetComponent<AudioSource>().clip = fightingClip;
            GetComponent<AudioSource>().Play();
        }
        else if (scene.name == "MainMenuScene")
        {
            GetComponent<AudioSource>().clip = menuClip;
            GetComponent<AudioSource>().Play();
        }
        else if (scene.name == "CharacterSelectScene")
        {

        }

    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    public void Play_UISound()
    {
        if (uiSound.isPlaying)
        {
            uiSound.Stop();
        }
        uiSound.Play();
    }

    public void Play_FoolWin()
    {
        if (foolWins.isPlaying)
        {
            foolWins.Stop();
        }
        foolWins.Play();
    }

    public void Play_FoolLaugh()
    {
        if (foolLaugh.isPlaying)
        {
            foolLaugh.Stop();
        }
        foolLaugh.Play();
    }

    public void Play_ReptileLick()
    {
        if (reptileLick.isPlaying)
        {
            reptileLick.Stop();
        }
        reptileLick.Play();
    }

    public void Play_CountDownFight()
    {

    }

    public void Play_VictoryLine(string winningFreak)
    {
        AudioSource tempFreakAudio = null;

        if (winningFreak == "TheFool")
        {
            Play_FoolWin();
        }
        else if (winningFreak == "Reptile")
        {
            Play_ReptileWin();
        }
        //StartCoroutine(playSoundsInOrder(tempPlayerAudio, tempFreakAudio));
    }

    private void Play_ReptileWin()
    {
        if (reptileWins.isPlaying)
        {
            reptileWins.Stop();
        }
        reptileWins.Play();
    }

    IEnumerator playSoundsInOrder(AudioSource winningPlayer, AudioSource winningFreak)
    {
        winningPlayer.Play();
        yield return new WaitForSeconds(winningPlayer.clip.length);
        winningFreak.Play();
    }

    public void StopAllSounds()
    {
        StopAllCoroutines();
        uiSound.Stop();
        foolWins.Stop();
        foolLaugh.Stop();
        reptileLick.Stop();
        player1Wins.Stop();
        player2Wins.Stop();
    }
}
