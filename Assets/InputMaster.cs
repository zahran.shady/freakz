// GENERATED AUTOMATICALLY FROM 'Assets/InputMaster.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @InputMaster : IInputActionCollection, IDisposable
{
    private InputActionAsset asset;
    public @InputMaster()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""InputMaster"",
    ""maps"": [
        {
            ""name"": ""Fighting"",
            ""id"": ""e1e2c54b-268d-48fc-adb4-ddf8d7f03675"",
            ""actions"": [
                {
                    ""name"": ""Jump"",
                    ""type"": ""Button"",
                    ""id"": ""44538b5f-9083-4164-a9ef-a2f2fcc8e44c"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""BasicAttack"",
                    ""type"": ""Button"",
                    ""id"": ""1d9e00e9-50a8-4fc5-8bdf-3d751ea9c829"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""SpawnPlayer"",
                    ""type"": ""Button"",
                    ""id"": ""c74955b5-abaa-4447-9532-496e220251f2"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Move"",
                    ""type"": ""Value"",
                    ""id"": ""21153087-5c8b-40bd-b833-9c6319b8d365"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Switch"",
                    ""type"": ""Button"",
                    ""id"": ""d026daeb-08b2-4cdf-8af5-d8af189b8cbd"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Block"",
                    ""type"": ""Button"",
                    ""id"": ""7e8f57cd-75d6-4f48-a1fc-0f50f9530ad5"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Mutate"",
                    ""type"": ""Button"",
                    ""id"": ""26586c3c-e3c7-430a-bc72-2b3a5e726aff"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""766524d8-a320-422d-a691-7ae66a165842"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""da49a1dd-2927-4859-8dc2-dd693ea8370c"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and mouse"",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3df06e7a-56b9-4d98-ab2a-5dc2b67031e9"",
                    ""path"": ""<Gamepad>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""BasicAttack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""84a02729-32fa-4466-9860-b51e0970d88c"",
                    ""path"": ""<Keyboard>/f"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and mouse"",
                    ""action"": ""BasicAttack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f392d5e8-91f0-4beb-b918-5779e7071d0e"",
                    ""path"": ""<Gamepad>/start"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""SpawnPlayer"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""0828f8dd-2ae2-47a3-8a27-010e9ad186ec"",
                    ""path"": ""<Keyboard>/enter"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and mouse"",
                    ""action"": ""SpawnPlayer"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""Gamepad"",
                    ""id"": ""2e8ef7fd-9793-459d-b8a1-701d003e4a68"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""5ab274f4-58e3-404b-adbd-cc436db7ead7"",
                    ""path"": ""<Gamepad>/leftStick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""d1ae74db-ddfe-4483-b3b0-966b44f6e080"",
                    ""path"": ""<Gamepad>/leftStick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Keyboard"",
                    ""id"": ""0ed89160-b103-4223-8c9d-4b039a92a6d6"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""23f93eaa-5647-4683-b9b4-83bae9e5e13d"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""fd8e7d3b-5a4b-4366-acd5-6e496672ff43"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""ed2f89e2-c974-44a5-aaed-5eca3657cce7"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Switch"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a9453c35-d267-4bc4-b825-4b3b81c64dc5"",
                    ""path"": ""<Keyboard>/r"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Switch"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""24cf9d99-7649-469c-b286-4ea3ba0b11a3"",
                    ""path"": ""<Gamepad>/leftTrigger"",
                    ""interactions"": ""Press(behavior=2)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Block"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e6b31400-07bc-4878-8e41-1ced4743a652"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": ""Press(behavior=2)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Block"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7a12139a-bcc4-4bcf-ac4c-91743f8bed5d"",
                    ""path"": ""<Gamepad>/buttonNorth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Mutate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f83f9f01-9077-4fca-bb50-9e4f23d199aa"",
                    ""path"": ""<Keyboard>/c"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Mutate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""FightEnd"",
            ""id"": ""9abdbf03-7ca2-4e4f-8ceb-8683ba96e8b5"",
            ""actions"": [
                {
                    ""name"": ""Skip"",
                    ""type"": ""Button"",
                    ""id"": ""eef58129-de41-4b0a-be49-58cd571ab25f"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""a25e24d9-884b-4785-bd35-5e0c4a9d9d6e"",
                    ""path"": ""<Gamepad>/start"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Skip"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c95e00c9-d358-467f-9399-c8fd609b74ff"",
                    ""path"": ""<Keyboard>/enter"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and mouse"",
                    ""action"": ""Skip"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Idle"",
            ""id"": ""22ed306c-37d6-4a3a-a5f2-92bc1966bb51"",
            ""actions"": [],
            ""bindings"": []
        },
        {
            ""name"": ""UI_CharacterMenu"",
            ""id"": ""03a85775-3935-48e4-9a7f-b972c29af06c"",
            ""actions"": [
                {
                    ""name"": ""SwitchCharacter"",
                    ""type"": ""Value"",
                    ""id"": ""f4a44b56-4e8c-42bf-a72c-df8066880d6a"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""AnyButton"",
                    ""type"": ""Button"",
                    ""id"": ""f5a91899-0cf2-481a-bb0e-5de36272bc99"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""Gamepad"",
                    ""id"": ""31656f72-2c06-4012-b6a4-306c8cc0bc21"",
                    ""path"": ""1DAxis"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SwitchCharacter"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""10c9fa1f-2ad9-4293-beaa-eb2bd4f280d8"",
                    ""path"": ""<Gamepad>/rightStick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""SwitchCharacter"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""a989fd90-bc9d-47eb-9d5b-63234975a7fd"",
                    ""path"": ""<Gamepad>/rightStick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""SwitchCharacter"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Keyboard"",
                    ""id"": ""fb5fdf8d-4e67-46d1-9b3d-3515945733c5"",
                    ""path"": ""1DAxis"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SwitchCharacter"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""c2498160-fbaa-4e7f-a32b-1f0058c99ed2"",
                    ""path"": ""<Keyboard>/#(A)"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and mouse"",
                    ""action"": ""SwitchCharacter"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""16e19749-0bc2-4d84-9034-76cb749ad349"",
                    ""path"": ""<Keyboard>/#(D)"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and mouse"",
                    ""action"": ""SwitchCharacter"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""14c70438-cc73-4f46-8e67-3802339bd300"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""AnyButton"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""96fd0682-71c0-4a8e-a932-34bf56398f02"",
                    ""path"": ""<Gamepad>/rightStick"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""AnyButton"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""35af411a-1aa7-438c-8912-1c421af81af6"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""AnyButton"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""825d46c6-7768-4689-b830-66de8cae93cf"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""AnyButton"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""11fef116-44d5-4178-b6e7-8b1d675eb320"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""AnyButton"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""28bea882-521e-4703-9997-7be3f9fb47c6"",
                    ""path"": ""<Keyboard>/enter"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""AnyButton"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a5551b98-7cd7-4464-980b-15439503a17c"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""AnyButton"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""eb8dfd96-e747-4ceb-9546-18e0d6af9f76"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""AnyButton"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""UI_MainMenu"",
            ""id"": ""93c0c433-5893-45a3-ab38-9176bef1ccca"",
            ""actions"": [
                {
                    ""name"": ""AnyButton"",
                    ""type"": ""Button"",
                    ""id"": ""2f32b0a4-c5a3-4519-9295-160a94da9d96"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""5ec21de9-0080-491a-950c-31ba39ead610"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""AnyButton"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a3cd7557-3f2d-4931-a075-65f1d42d9bc3"",
                    ""path"": ""<Gamepad>/rightStick"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""AnyButton"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""60d9dabc-b179-4c0e-967b-82c99b4bd280"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""AnyButton"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""88031c5a-5083-4b25-bf42-aeef182b91d2"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""AnyButton"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1d3bb699-bee9-41a1-99f0-d7efa1d2553b"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""AnyButton"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b722ab6b-d119-43ee-a9ee-830156d08bfc"",
                    ""path"": ""<Keyboard>/enter"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""AnyButton"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""Gamepad"",
            ""bindingGroup"": ""Gamepad"",
            ""devices"": [
                {
                    ""devicePath"": ""<XInputController>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        },
        {
            ""name"": ""Keyboard and mouse"",
            ""bindingGroup"": ""Keyboard and mouse"",
            ""devices"": [
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                },
                {
                    ""devicePath"": ""<Mouse>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
        // Fighting
        m_Fighting = asset.FindActionMap("Fighting", throwIfNotFound: true);
        m_Fighting_Jump = m_Fighting.FindAction("Jump", throwIfNotFound: true);
        m_Fighting_BasicAttack = m_Fighting.FindAction("BasicAttack", throwIfNotFound: true);
        m_Fighting_SpawnPlayer = m_Fighting.FindAction("SpawnPlayer", throwIfNotFound: true);
        m_Fighting_Move = m_Fighting.FindAction("Move", throwIfNotFound: true);
        m_Fighting_Switch = m_Fighting.FindAction("Switch", throwIfNotFound: true);
        m_Fighting_Block = m_Fighting.FindAction("Block", throwIfNotFound: true);
        m_Fighting_Mutate = m_Fighting.FindAction("Mutate", throwIfNotFound: true);
        // FightEnd
        m_FightEnd = asset.FindActionMap("FightEnd", throwIfNotFound: true);
        m_FightEnd_Skip = m_FightEnd.FindAction("Skip", throwIfNotFound: true);
        // Idle
        m_Idle = asset.FindActionMap("Idle", throwIfNotFound: true);
        // UI_CharacterMenu
        m_UI_CharacterMenu = asset.FindActionMap("UI_CharacterMenu", throwIfNotFound: true);
        m_UI_CharacterMenu_SwitchCharacter = m_UI_CharacterMenu.FindAction("SwitchCharacter", throwIfNotFound: true);
        m_UI_CharacterMenu_AnyButton = m_UI_CharacterMenu.FindAction("AnyButton", throwIfNotFound: true);
        // UI_MainMenu
        m_UI_MainMenu = asset.FindActionMap("UI_MainMenu", throwIfNotFound: true);
        m_UI_MainMenu_AnyButton = m_UI_MainMenu.FindAction("AnyButton", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Fighting
    private readonly InputActionMap m_Fighting;
    private IFightingActions m_FightingActionsCallbackInterface;
    private readonly InputAction m_Fighting_Jump;
    private readonly InputAction m_Fighting_BasicAttack;
    private readonly InputAction m_Fighting_SpawnPlayer;
    private readonly InputAction m_Fighting_Move;
    private readonly InputAction m_Fighting_Switch;
    private readonly InputAction m_Fighting_Block;
    private readonly InputAction m_Fighting_Mutate;
    public struct FightingActions
    {
        private @InputMaster m_Wrapper;
        public FightingActions(@InputMaster wrapper) { m_Wrapper = wrapper; }
        public InputAction @Jump => m_Wrapper.m_Fighting_Jump;
        public InputAction @BasicAttack => m_Wrapper.m_Fighting_BasicAttack;
        public InputAction @SpawnPlayer => m_Wrapper.m_Fighting_SpawnPlayer;
        public InputAction @Move => m_Wrapper.m_Fighting_Move;
        public InputAction @Switch => m_Wrapper.m_Fighting_Switch;
        public InputAction @Block => m_Wrapper.m_Fighting_Block;
        public InputAction @Mutate => m_Wrapper.m_Fighting_Mutate;
        public InputActionMap Get() { return m_Wrapper.m_Fighting; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(FightingActions set) { return set.Get(); }
        public void SetCallbacks(IFightingActions instance)
        {
            if (m_Wrapper.m_FightingActionsCallbackInterface != null)
            {
                @Jump.started -= m_Wrapper.m_FightingActionsCallbackInterface.OnJump;
                @Jump.performed -= m_Wrapper.m_FightingActionsCallbackInterface.OnJump;
                @Jump.canceled -= m_Wrapper.m_FightingActionsCallbackInterface.OnJump;
                @BasicAttack.started -= m_Wrapper.m_FightingActionsCallbackInterface.OnBasicAttack;
                @BasicAttack.performed -= m_Wrapper.m_FightingActionsCallbackInterface.OnBasicAttack;
                @BasicAttack.canceled -= m_Wrapper.m_FightingActionsCallbackInterface.OnBasicAttack;
                @SpawnPlayer.started -= m_Wrapper.m_FightingActionsCallbackInterface.OnSpawnPlayer;
                @SpawnPlayer.performed -= m_Wrapper.m_FightingActionsCallbackInterface.OnSpawnPlayer;
                @SpawnPlayer.canceled -= m_Wrapper.m_FightingActionsCallbackInterface.OnSpawnPlayer;
                @Move.started -= m_Wrapper.m_FightingActionsCallbackInterface.OnMove;
                @Move.performed -= m_Wrapper.m_FightingActionsCallbackInterface.OnMove;
                @Move.canceled -= m_Wrapper.m_FightingActionsCallbackInterface.OnMove;
                @Switch.started -= m_Wrapper.m_FightingActionsCallbackInterface.OnSwitch;
                @Switch.performed -= m_Wrapper.m_FightingActionsCallbackInterface.OnSwitch;
                @Switch.canceled -= m_Wrapper.m_FightingActionsCallbackInterface.OnSwitch;
                @Block.started -= m_Wrapper.m_FightingActionsCallbackInterface.OnBlock;
                @Block.performed -= m_Wrapper.m_FightingActionsCallbackInterface.OnBlock;
                @Block.canceled -= m_Wrapper.m_FightingActionsCallbackInterface.OnBlock;
                @Mutate.started -= m_Wrapper.m_FightingActionsCallbackInterface.OnMutate;
                @Mutate.performed -= m_Wrapper.m_FightingActionsCallbackInterface.OnMutate;
                @Mutate.canceled -= m_Wrapper.m_FightingActionsCallbackInterface.OnMutate;
            }
            m_Wrapper.m_FightingActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Jump.started += instance.OnJump;
                @Jump.performed += instance.OnJump;
                @Jump.canceled += instance.OnJump;
                @BasicAttack.started += instance.OnBasicAttack;
                @BasicAttack.performed += instance.OnBasicAttack;
                @BasicAttack.canceled += instance.OnBasicAttack;
                @SpawnPlayer.started += instance.OnSpawnPlayer;
                @SpawnPlayer.performed += instance.OnSpawnPlayer;
                @SpawnPlayer.canceled += instance.OnSpawnPlayer;
                @Move.started += instance.OnMove;
                @Move.performed += instance.OnMove;
                @Move.canceled += instance.OnMove;
                @Switch.started += instance.OnSwitch;
                @Switch.performed += instance.OnSwitch;
                @Switch.canceled += instance.OnSwitch;
                @Block.started += instance.OnBlock;
                @Block.performed += instance.OnBlock;
                @Block.canceled += instance.OnBlock;
                @Mutate.started += instance.OnMutate;
                @Mutate.performed += instance.OnMutate;
                @Mutate.canceled += instance.OnMutate;
            }
        }
    }
    public FightingActions @Fighting => new FightingActions(this);

    // FightEnd
    private readonly InputActionMap m_FightEnd;
    private IFightEndActions m_FightEndActionsCallbackInterface;
    private readonly InputAction m_FightEnd_Skip;
    public struct FightEndActions
    {
        private @InputMaster m_Wrapper;
        public FightEndActions(@InputMaster wrapper) { m_Wrapper = wrapper; }
        public InputAction @Skip => m_Wrapper.m_FightEnd_Skip;
        public InputActionMap Get() { return m_Wrapper.m_FightEnd; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(FightEndActions set) { return set.Get(); }
        public void SetCallbacks(IFightEndActions instance)
        {
            if (m_Wrapper.m_FightEndActionsCallbackInterface != null)
            {
                @Skip.started -= m_Wrapper.m_FightEndActionsCallbackInterface.OnSkip;
                @Skip.performed -= m_Wrapper.m_FightEndActionsCallbackInterface.OnSkip;
                @Skip.canceled -= m_Wrapper.m_FightEndActionsCallbackInterface.OnSkip;
            }
            m_Wrapper.m_FightEndActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Skip.started += instance.OnSkip;
                @Skip.performed += instance.OnSkip;
                @Skip.canceled += instance.OnSkip;
            }
        }
    }
    public FightEndActions @FightEnd => new FightEndActions(this);

    // Idle
    private readonly InputActionMap m_Idle;
    private IIdleActions m_IdleActionsCallbackInterface;
    public struct IdleActions
    {
        private @InputMaster m_Wrapper;
        public IdleActions(@InputMaster wrapper) { m_Wrapper = wrapper; }
        public InputActionMap Get() { return m_Wrapper.m_Idle; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(IdleActions set) { return set.Get(); }
        public void SetCallbacks(IIdleActions instance)
        {
            if (m_Wrapper.m_IdleActionsCallbackInterface != null)
            {
            }
            m_Wrapper.m_IdleActionsCallbackInterface = instance;
            if (instance != null)
            {
            }
        }
    }
    public IdleActions @Idle => new IdleActions(this);

    // UI_CharacterMenu
    private readonly InputActionMap m_UI_CharacterMenu;
    private IUI_CharacterMenuActions m_UI_CharacterMenuActionsCallbackInterface;
    private readonly InputAction m_UI_CharacterMenu_SwitchCharacter;
    private readonly InputAction m_UI_CharacterMenu_AnyButton;
    public struct UI_CharacterMenuActions
    {
        private @InputMaster m_Wrapper;
        public UI_CharacterMenuActions(@InputMaster wrapper) { m_Wrapper = wrapper; }
        public InputAction @SwitchCharacter => m_Wrapper.m_UI_CharacterMenu_SwitchCharacter;
        public InputAction @AnyButton => m_Wrapper.m_UI_CharacterMenu_AnyButton;
        public InputActionMap Get() { return m_Wrapper.m_UI_CharacterMenu; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(UI_CharacterMenuActions set) { return set.Get(); }
        public void SetCallbacks(IUI_CharacterMenuActions instance)
        {
            if (m_Wrapper.m_UI_CharacterMenuActionsCallbackInterface != null)
            {
                @SwitchCharacter.started -= m_Wrapper.m_UI_CharacterMenuActionsCallbackInterface.OnSwitchCharacter;
                @SwitchCharacter.performed -= m_Wrapper.m_UI_CharacterMenuActionsCallbackInterface.OnSwitchCharacter;
                @SwitchCharacter.canceled -= m_Wrapper.m_UI_CharacterMenuActionsCallbackInterface.OnSwitchCharacter;
                @AnyButton.started -= m_Wrapper.m_UI_CharacterMenuActionsCallbackInterface.OnAnyButton;
                @AnyButton.performed -= m_Wrapper.m_UI_CharacterMenuActionsCallbackInterface.OnAnyButton;
                @AnyButton.canceled -= m_Wrapper.m_UI_CharacterMenuActionsCallbackInterface.OnAnyButton;
            }
            m_Wrapper.m_UI_CharacterMenuActionsCallbackInterface = instance;
            if (instance != null)
            {
                @SwitchCharacter.started += instance.OnSwitchCharacter;
                @SwitchCharacter.performed += instance.OnSwitchCharacter;
                @SwitchCharacter.canceled += instance.OnSwitchCharacter;
                @AnyButton.started += instance.OnAnyButton;
                @AnyButton.performed += instance.OnAnyButton;
                @AnyButton.canceled += instance.OnAnyButton;
            }
        }
    }
    public UI_CharacterMenuActions @UI_CharacterMenu => new UI_CharacterMenuActions(this);

    // UI_MainMenu
    private readonly InputActionMap m_UI_MainMenu;
    private IUI_MainMenuActions m_UI_MainMenuActionsCallbackInterface;
    private readonly InputAction m_UI_MainMenu_AnyButton;
    public struct UI_MainMenuActions
    {
        private @InputMaster m_Wrapper;
        public UI_MainMenuActions(@InputMaster wrapper) { m_Wrapper = wrapper; }
        public InputAction @AnyButton => m_Wrapper.m_UI_MainMenu_AnyButton;
        public InputActionMap Get() { return m_Wrapper.m_UI_MainMenu; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(UI_MainMenuActions set) { return set.Get(); }
        public void SetCallbacks(IUI_MainMenuActions instance)
        {
            if (m_Wrapper.m_UI_MainMenuActionsCallbackInterface != null)
            {
                @AnyButton.started -= m_Wrapper.m_UI_MainMenuActionsCallbackInterface.OnAnyButton;
                @AnyButton.performed -= m_Wrapper.m_UI_MainMenuActionsCallbackInterface.OnAnyButton;
                @AnyButton.canceled -= m_Wrapper.m_UI_MainMenuActionsCallbackInterface.OnAnyButton;
            }
            m_Wrapper.m_UI_MainMenuActionsCallbackInterface = instance;
            if (instance != null)
            {
                @AnyButton.started += instance.OnAnyButton;
                @AnyButton.performed += instance.OnAnyButton;
                @AnyButton.canceled += instance.OnAnyButton;
            }
        }
    }
    public UI_MainMenuActions @UI_MainMenu => new UI_MainMenuActions(this);
    private int m_GamepadSchemeIndex = -1;
    public InputControlScheme GamepadScheme
    {
        get
        {
            if (m_GamepadSchemeIndex == -1) m_GamepadSchemeIndex = asset.FindControlSchemeIndex("Gamepad");
            return asset.controlSchemes[m_GamepadSchemeIndex];
        }
    }
    private int m_KeyboardandmouseSchemeIndex = -1;
    public InputControlScheme KeyboardandmouseScheme
    {
        get
        {
            if (m_KeyboardandmouseSchemeIndex == -1) m_KeyboardandmouseSchemeIndex = asset.FindControlSchemeIndex("Keyboard and mouse");
            return asset.controlSchemes[m_KeyboardandmouseSchemeIndex];
        }
    }
    public interface IFightingActions
    {
        void OnJump(InputAction.CallbackContext context);
        void OnBasicAttack(InputAction.CallbackContext context);
        void OnSpawnPlayer(InputAction.CallbackContext context);
        void OnMove(InputAction.CallbackContext context);
        void OnSwitch(InputAction.CallbackContext context);
        void OnBlock(InputAction.CallbackContext context);
        void OnMutate(InputAction.CallbackContext context);
    }
    public interface IFightEndActions
    {
        void OnSkip(InputAction.CallbackContext context);
    }
    public interface IIdleActions
    {
    }
    public interface IUI_CharacterMenuActions
    {
        void OnSwitchCharacter(InputAction.CallbackContext context);
        void OnAnyButton(InputAction.CallbackContext context);
    }
    public interface IUI_MainMenuActions
    {
        void OnAnyButton(InputAction.CallbackContext context);
    }
}
