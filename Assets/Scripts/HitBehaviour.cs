﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitBehaviour : MonoBehaviour
{
    public AttackType myAttackType;
    private Freak m_freak;
    private void Awake()
    {
        m_freak = transform.parent.GetComponent<Freak>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Freak>() != null)
        {
            //Debug.Log(" I am: " + transform.parent.parent.name);
            if (collision == transform.parent.GetComponent<Collider2D>())
            {
                //Debug.Log(collision.name + "owner Inside hit");
            }


            else
            {
                //Debug.Log(collision.name + "stranger Inside hit");
                if (transform.parent.position.x <= collision.transform.position.x)
                {
                    if (myAttackType == AttackType.BasicAttack)
                    {
                        collision.GetComponent<Freak>().OnHit(m_freak.basicAttackDamage, 1);
                    }
                    else if (myAttackType == AttackType.MutationAttack)
                    {
                        collision.GetComponent<Freak>().OnHit(m_freak.mutationAttackDamage, 1);
                    }

                    if (collision.GetComponent<Freak>().m_playercontroller.IsBlockingSameDirectionAsHit(1) && collision.GetComponent<Freak>().m_playercontroller.Currentstate == PlayerController.PlayerState.Blocking)
                    {
                        m_freak.Charge += 5;
                    }
                    else if (!collision.GetComponent<Freak>().m_playercontroller.isInvulnerable)
                    {
                        m_freak.Charge += 20;
                    }
                }
                else
                {
                    if (myAttackType == AttackType.BasicAttack)
                    {
                        collision.GetComponent<Freak>().OnHit(m_freak.basicAttackDamage, -1);
                    }
                    else if (myAttackType == AttackType.MutationAttack)
                    {
                        collision.GetComponent<Freak>().OnHit(m_freak.mutationAttackDamage, -1);
                    }

                    if (collision.GetComponent<Freak>().m_playercontroller.IsBlockingSameDirectionAsHit(-1) && collision.GetComponent<Freak>().m_playercontroller.Currentstate == PlayerController.PlayerState.Blocking)
                    {
                        m_freak.Charge += 5;
                    }
                    else if (!collision.GetComponent<Freak>().m_playercontroller.isInvulnerable)
                    {
                        m_freak.Charge += 20;
                    }
                }

                m_freak.PlayHitEffectAnimation();
            }

        }
    }

    public enum AttackType
    {
        BasicAttack,
        MutationAttack
    }
}
